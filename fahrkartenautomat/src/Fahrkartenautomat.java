import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
        Scanner tastatur = new Scanner(System.in);
        // Achtung!!!!
        // Hier wird deutlich, warum double nicht für Geldbeträge geeignet ist.
        // =======================================
        double zuZahlenderBetrag;
        double eingezahlterGesamtbetrag;
        double eingeworfeneMünze;
        double rückgabebetrag;
        // Die Eingabe erfolgt anwenderfreudlich mit Dezimalpunkt: just testing
        double eingegebenerBetrag;
        double ticketPreis;
        int anzahlderTickets;

        zuZahlenderBetrag = fahrkartenbestellungErfassen();
        rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        fahrkartenAusgeben();
        rueckgeldAusgeben(rückgabebetrag);
    }

    public static double fahrkartenbestellungErfassen(){
        // Den zu zahlenden Betrag ermittelt normalerweise der Automat
        // aufgrund der gewählten Fahrkarte(n).
        // -----------------------------------
        String[] ticketBezeichnung = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
        double[] ticketPreise = {2.9, 3.3, 3.6, 1.9, 8.6, 9, 9.6, 23.5, 24.3, 24.9};
        Scanner tastatur = new Scanner(System.in);
        for(int i = 0; i<ticketBezeichnung.length; i++){
            System.out.printf("%d\t%-40s%.2f€\n", i+1, ticketBezeichnung[i], ticketPreise[i]);
        }
        System.out.print("Auswahl: ");
        int ticketArt = tastatur.nextInt();
        double ticketPreis = ticketPreise[ticketArt-1];
        System.out.print("Anzahl der Tickets: ");
        double anzahlderTickets = tastatur.nextInt();
        double zuZahlenderBetrag = anzahlderTickets * ticketPreis;
        return zuZahlenderBetrag;
    }

    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        // Geldeinwurf
        // -----------
        Scanner tastatur = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
            System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            double eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        double rückgeld = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        return rückgeld;
    }

    public static void fahrkartenAusgeben(){
        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public static void rueckgeldAusgeben(double rückgabebetrag) {

        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        if(rückgabebetrag > 0.0)
        {
            System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rückgabebetrag);
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
    }
}