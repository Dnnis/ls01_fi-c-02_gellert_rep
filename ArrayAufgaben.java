import java.util.Arrays;
import java.util.Scanner;

public class ArrayAufgaben {

    public static void mains1 (String[] args) {



        int[] zlist = new int[10];


        int currentindex = 0;
        for(int i = 0; i < 20; i++) {
            if(currentindex >= zlist.length) break;
            if(i %2 == 1) {
                zlist[currentindex] = i;
                currentindex++;
            }
        }

        for(int i : zlist) {
            System.out.println(i);
        }

    }

    public static void mainss (String[] args) {

        Scanner sc = new Scanner(System.in);
        int[] arr = new int[5];

        for(int i=0; i < 5; i++) {
            arr[i] = sc.nextInt();
        }

        for(int i = arr.length -1; i >= 0; i--) {
            System.out.println(arr[i]);
        }
    }

    public static void main (String[] args){
        int Lotto[] = new int[6];
        Lotto[0] = 3;
        Lotto[1] = 7;
        Lotto[2] = 17;
        Lotto[3] = 18;
        Lotto[4] = 37;
        Lotto[5] = 42;

        System.out.println("[ ");

        for(int a : Lotto) {
            System.out.print(a + " ");
        }
        System.out.print("]\n");

        boolean zwölfGefunden = false;

        for( int a : Lotto) {
            if(a == 12){
                zwölfGefunden = true;
            }
        }
        if(zwölfGefunden) {
            System.out.println("Die Zahl ist in der Ziehung enthalten.");
        } else {
            System.out.println("Die Zahl ist nicht in der Ziehung enthalten.");
        }
    }
}

